/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import pockermen.FXMLEditTaskController;
import static com.sun.javafx.scene.control.skin.Utils.getResource;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import pockermen.Provider;
import pockermen.Singleton;
import pockermen.StatusStake;
import pockermen.database.Providers;
import pockermen.database.Stakelist;

/**
 *
 * @author user
 */
public class FXMLMainController implements Initializable {

    private ObservableList<Stakelist> olStake;

    private Stage stage;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private MenuItem miExit;

    @FXML
    private Menu mEdit;

    @FXML
    private Button tbAddTask;

    @FXML
    private Button tbEditTask;

    @FXML
    private Button tbDelTask;

    @FXML
    private ListView<String> lvInfo;

    @FXML
    private TableView<Stakelist> tvStake;

    @FXML
    private TableColumn<Stakelist, Date> tcDate;

    @FXML
    private TableColumn<Stakelist, Date> tcDateEnd;

    @FXML
    private TableColumn<Stakelist, String> tcAuction;

    @FXML
    private TableColumn<Stakelist, String> tcProvider;

    @FXML
    private TableColumn<Stakelist, String> tcECP;

    @FXML
    private TableColumn<Stakelist, String> tcAlgoritm;

    @FXML
    private TableColumn<Stakelist, String> tcContractor;

    @FXML
    private TableColumn<Stakelist, String> tcPushName;

    @FXML
    private TableColumn<Stakelist, BigDecimal> tcPrice;

    @FXML
    private TableColumn<Stakelist, BigDecimal> tcMaxPrice;

    @FXML
    private TableColumn<Stakelist, BigDecimal> tcWinnerPrice;

    @FXML
    private TableColumn<Stakelist, String> tcStatus;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //tbAddTask.graphicProperty().setValue(new ImageView(new Image("img/Symbol Add 2.png")));

        initButton(tbAddTask, "img/Symbol Add 2.png");
        initButton(tbEditTask, "img/Symbol Edit 2.png");
        initButton(tbDelTask, "img/Symbol Restricted 2.png");
        tbAddTask.setDisable(false);
        tbEditTask.setDisable(false);
        mEdit.setDisable(true);

        // заполняем таблицу данными
        tcDate.setCellValueFactory(new PropertyValueFactory<>("DateStart"));
        tcDate.setCellFactory(new ToolTipCellFactory<>());
        tcDateEnd.setCellValueFactory(new PropertyValueFactory<>("DateEnd"));
        tcDateEnd.setCellFactory(new ToolTipCellFactory<>());
        tcAuction.setCellValueFactory(new PropertyValueFactory<>("Auction"));
        tcAuction.setCellFactory(new ToolTipCellFactory<>());
        tcProvider.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdProvider().getPname()));
        tcProvider.setCellFactory(new ToolTipCellFactory<>());
        tcECP.setCellValueFactory(new PropertyValueFactory<>("Ecp"));
        tcECP.setCellFactory(new ToolTipCellFactory<>());
        tcAlgoritm.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdAlg().getAlgname()));
        tcAlgoritm.setCellFactory(new ToolTipCellFactory<>());
        tcContractor.setCellValueFactory(new PropertyValueFactory<>("Contractor"));
        tcContractor.setCellFactory(new ToolTipCellFactory<>());
        tcPushName.setCellValueFactory(new PropertyValueFactory<>("PurchName"));
        tcPushName.setCellFactory(new ToolTipCellFactory<>());
        tcPrice.setCellValueFactory(new PropertyValueFactory<>("Price"));
        tcPrice.setCellFactory(new ToolTipCellFactory<>());
        tcMaxPrice.setCellValueFactory(new PropertyValueFactory<>("Maxprice"));
        tcMaxPrice.setCellFactory(new ToolTipCellFactory<>());
        tcWinnerPrice.setCellValueFactory(new PropertyValueFactory<>("WinnerPrice"));
        tcWinnerPrice.setCellFactory(new ToolTipCellFactory<>());
        tcStatus.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdStatus().getSname()));
        tcStatus.setCellFactory(new ToolTipCellStatusFactory<>());

        olStake = Singleton.getInstance().getStakeList();
//        this.corporationsSumTableColumn.setCellValueFactory(cellData -> cellData.getValue().getSum());
        tvStake.setItems(olStake);
        StatusStake.loadStatusList();

        Platform.runLater(()
                -> lvInfo.lookupAll(".scroll-bar").stream()
                        .filter(br -> Objects.equals(Orientation.VERTICAL, ((ScrollBar) br).getOrientation()))
                        .findFirst().ifPresent(br
                                -> br.visibleProperty().addListener((observable, oldValue, newValue)
                                -> lvInfo.scrollTo(newValue ? Integer.MAX_VALUE : 0))));
    }

    public class ToolTipCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

        @Override
        public TableCell<S, T> call(TableColumn<S, T> param) {
            return new TableCell<S, T>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    //Здесь необходимо установить текст ячейки
                    //И заодно текст всплывающей подсказки
                    if (item == null) {
                        setTooltip(null);
                        setText(null);
                    } else {
                        setTooltip(new Tooltip(item.toString()));
                        setText(item.toString());
                    }
                }
            };
        }
    }

    public class ToolTipCellStatusFactory<Stakelist, T> implements Callback<TableColumn<Stakelist, T>, TableCell<Stakelist, T>> {

        @Override
        public TableCell<Stakelist, T> call(TableColumn<Stakelist, T> param) {
            return new TableCell<Stakelist, T>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    //Здесь необходимо установить текст ячейки
                    //И заодно текст всплывающей подсказки
                    if (item == null) {
                        setTooltip(null);
                        setText(null);
                    } else {
                        //String sid 
                        int index = getIndex();
                        pockermen.database.Stakelist stakelist = (pockermen.database.Stakelist) getTableView().getItems().get(index);

                        String stcomment = stakelist.getStatusComment();
                        if (stcomment != null) {
                            int idStatus = stakelist.getIdStatus().getId();
                            String sIdStatus = String.valueOf(idStatus);
                            if (stcomment.startsWith(sIdStatus + ":")) {
                                stcomment = stcomment.substring(sIdStatus.length() + 1);
                                String comment = item + "\n" + stcomment;
                                setTooltip(new Tooltip(comment));
                            }
                        }
                        setText(item.toString());
                    }
                }
            };
        }
    }

    private void initButton(Button bt, String imgsrc) {
        ImageView iv = new ImageView(new Image(imgsrc));
        iv.setFitHeight(20);
        iv.setFitWidth(20);
        bt.textProperty().setValue("");
        bt.graphicProperty().setValue(iv);
        bt.setMaxWidth(30);
    }

    @FXML
    private void handleAddTask(ActionEvent event) throws IOException {
        System.out.println("You clicked handleAddTask!");
        Stakelist stake = new Stakelist(0);
        createEditDialog(stake, "FXMLEditTask.fxml");
    }

    @FXML
    private void handleEditTask(ActionEvent event) throws IOException {
        System.out.println("You clicked handleEditTask!");
        Stakelist stk = tvStake.getSelectionModel().getSelectedItem();
        if (stk != null) {
            createEditDialog(stk, "FXMLEditTask.fxml");
        }
    }

    @FXML
    private void handleDelTask(ActionEvent event) {
////        String userData = "C:\\Users\\Evgeniy\\AppData\\Local\\Google\\Chrome\\User Data";
//        System.out.println("You clicked handleDelTask!");
//        System.setProperty("webdriver.chrome.driver", "lib\\chromedriver_win32\\chromedriver.exe");
//        ChromeOptions option = new ChromeOptions();
//        option.addArguments("--window-size=500,500");
////        option.addArguments("user-data-dir="+userData);
////        option.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//        //option.addArguments("–load-extension=" + userData + "\\Default\\Extensions\\iifchhfnnmpdbibifmljnfjhpififfog\\1.2.4_0");
//        option.addExtensions(new File("plugin\\CryptoPro.crx"));
//
////        option.addArguments("headless");                //Hidden browser
//        ChromeDriver webDriver = new ChromeDriver(option);
//        //webDriver.get("chrome://extensions/");
    }

    @FXML
    private void handleExit(ActionEvent event) {
        stage.getOnCloseRequest().handle(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        stage.hide();
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private FXMLEditTaskController createEditDialog(Stakelist stake, final String fxmlString) throws IOException {
        FXMLLoader fl = new FXMLLoader();
        fl.setLocation(getClass().getResource("FXMLEditTask.fxml"));
        fl.load();
        Parent root = fl.getRoot();
        Stage modal_dialog = new Stage(StageStyle.DECORATED);
        modal_dialog.initModality(Modality.WINDOW_MODAL);
        modal_dialog.initOwner(stage);
        modal_dialog.setResizable(false);
        Scene scene = new Scene(root);

        FXMLEditTaskController t1 = (FXMLEditTaskController) fl.getController();
        t1.setStage(modal_dialog);
        t1.seFormFromtStake(stake);
        String tittle = (stake.getId() == 0) ? "Добавление ставки" : "Редактирование ставки";
        modal_dialog.setTitle(tittle);
        modal_dialog.setScene(scene);
        modal_dialog.show();
        return t1;
    }

    public synchronized void setStatus(String textState) {
        Platform.runLater(() -> {
            lvInfo.getItems().add(textState);
        });

    }

    public synchronized void addStake(Stakelist newstake) {
        Platform.runLater(() -> {
            olStake.add(newstake);
        });

    }

    public synchronized void refreshData() {
        Platform.runLater(tvStake::refresh);
    }

    public synchronized void enableAddButton(boolean isEnable) {
        Platform.runLater(() -> {
            tbAddTask.setDisable(!isEnable);
        });
    }

    public synchronized void enableEditButton(boolean isEnable) {
        Platform.runLater(() -> {
            tbEditTask.setDisable(!isEnable);
        });
    }

    public synchronized void enableMenuEdit(boolean isEnable) {
        Platform.runLater(() -> {
            mEdit.setDisable(!isEnable);
        });
    }

}
