/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.database;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Evgeniy
 */
@Entity
@Table(name = "algoritm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Algoritm.findAll", query = "SELECT a FROM Algoritm a")
    , @NamedQuery(name = "Algoritm.findById", query = "SELECT a FROM Algoritm a WHERE a.id = :id")
    , @NamedQuery(name = "Algoritm.findByAlgname", query = "SELECT a FROM Algoritm a WHERE a.algname = :algname")})
public class Algoritm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "algname")
    private String algname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAlg")
    private Collection<Stakelist> stakelistCollection;

    public Algoritm() {
    }

    public Algoritm(Integer id) {
        this.id = id;
    }

    public Algoritm(Integer id, String algname) {
        this.id = id;
        this.algname = algname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlgname() {
        return algname;
    }

    public void setAlgname(String algname) {
        this.algname = algname;
    }

    @XmlTransient
    public Collection<Stakelist> getStakelistCollection() {
        return stakelistCollection;
    }

    public void setStakelistCollection(Collection<Stakelist> stakelistCollection) {
        this.stakelistCollection = stakelistCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Algoritm)) {
            return false;
        }
        Algoritm other = (Algoritm) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pockermen.database.Algoritm[ id=" + id + " ]";
    }
    
}
