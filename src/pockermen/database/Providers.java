/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.database;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Evgeniy
 */
@Entity
@Table(name = "providers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Providers.findAll", query = "SELECT p FROM Providers p")
    , @NamedQuery(name = "Providers.findById", query = "SELECT p FROM Providers p WHERE p.id = :id")
    , @NamedQuery(name = "Providers.findByPname", query = "SELECT p FROM Providers p WHERE p.pname = :pname")})
public class Providers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "pname")
    private String pname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProvider")
    private Collection<Stakelist> stakelistCollection;

    public Providers() {
    }

    public Providers(Integer id) {
        this.id = id;
    }

    public Providers(Integer id, String pname) {
        this.id = id;
        this.pname = pname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    @XmlTransient
    public Collection<Stakelist> getStakelistCollection() {
        return stakelistCollection;
    }

    public void setStakelistCollection(Collection<Stakelist> stakelistCollection) {
        this.stakelistCollection = stakelistCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Providers)) {
            return false;
        }
        Providers other = (Providers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pockermen.database.Providers[ id=" + id + " ]";
    }
    
}
