/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Evgeniy
 */
@Entity
@Table(name = "stakelist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stakelist.findAll", query = "SELECT s FROM Stakelist s")
    , @NamedQuery(name = "Stakelist.findById", query = "SELECT s FROM Stakelist s WHERE s.id = :id")
    , @NamedQuery(name = "Stakelist.findByAuction", query = "SELECT s FROM Stakelist s WHERE s.auction = :auction")
    , @NamedQuery(name = "Stakelist.findByEcp", query = "SELECT s FROM Stakelist s WHERE s.ecp = :ecp")
    , @NamedQuery(name = "Stakelist.findByPrice", query = "SELECT s FROM Stakelist s WHERE s.price = :price")
    , @NamedQuery(name = "Stakelist.findByDateStart", query = "SELECT s FROM Stakelist s WHERE s.dateStart = :dateStart")
    , @NamedQuery(name = "Stakelist.findByIsWinner", query = "SELECT s FROM Stakelist s WHERE s.isWinner = :isWinner")
    , @NamedQuery(name = "Stakelist.findByWinnerPrice", query = "SELECT s FROM Stakelist s WHERE s.winnerPrice = :winnerPrice")
    , @NamedQuery(name = "Stakelist.findByContractor", query = "SELECT s FROM Stakelist s WHERE s.contractor = :contractor")
    , @NamedQuery(name = "Stakelist.findByStatusComment", query = "SELECT s FROM Stakelist s WHERE s.statusComment = :statusComment")
    , @NamedQuery(name = "Stakelist.findByPurchID", query = "SELECT s FROM Stakelist s WHERE s.purchID = :purchID")
    , @NamedQuery(name = "Stakelist.findByAsid", query = "SELECT s FROM Stakelist s WHERE s.asid = :asid")
    , @NamedQuery(name = "Stakelist.findByPurchName", query = "SELECT s FROM Stakelist s WHERE s.purchName = :purchName")
    , @NamedQuery(name = "Stakelist.findByReqID", query = "SELECT s FROM Stakelist s WHERE s.reqID = :reqID")
    , @NamedQuery(name = "Stakelist.findByMaxprice", query = "SELECT s FROM Stakelist s WHERE s.maxprice = :maxprice")
    , @NamedQuery(name = "Stakelist.findByDateEnd", query = "SELECT s FROM Stakelist s WHERE s.dateEnd = :dateEnd")
    , @NamedQuery(name = "Stakelist.findByPurchCode", query = "SELECT s FROM Stakelist s WHERE s.purchCode = :purchCode")})
public class Stakelist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "auction")
    private String auction;
    @Basic(optional = false)
    @Column(name = "ecp")
    private String ecp;
    @Basic(optional = false)
    @Column(name = "price")
    private double price;
    @Column(name = "date_start")
    private String dateStart;
    @Basic(optional = false)
    @Column(name = "is_winner")
    private int isWinner;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "winner_price")
    private Double winnerPrice;
    @Column(name = "contractor")
    private String contractor;
    @Column(name = "StatusComment")
    private String statusComment;
    @Column(name = "purchID")
    private String purchID;
    @Column(name = "ASID")
    private String asid;
    @Column(name = "purchName")
    private String purchName;
    @Column(name = "reqID")
    private String reqID;
    @Column(name = "maxprice")
    private Double maxprice;
    @Column(name = "date_end")
    private String dateEnd;
    @Column(name = "purchCode")
    private String purchCode;
    @JoinColumn(name = "id_alg", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Algoritm idAlg;
    @JoinColumn(name = "id_provider", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Providers idProvider;
    @JoinColumn(name = "id_status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Stakestatus idStatus;

    public Stakelist() {
    }

    public Stakelist(Integer id) {
        this.id = id;
    }

    public Stakelist(Integer id, String auction, String ecp, double price, int isWinner) {
        this.id = id;
        this.auction = auction;
        this.ecp = ecp;
        this.price = price;
        this.isWinner = isWinner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuction() {
        return auction;
    }

    public void setAuction(String auction) {
        this.auction = auction;
    }

    public String getEcp() {
        return ecp;
    }

    public void setEcp(String ecp) {
        this.ecp = ecp;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public int getIsWinner() {
        return isWinner;
    }

    public void setIsWinner(int isWinner) {
        this.isWinner = isWinner;
    }

    public Double getWinnerPrice() {
        return winnerPrice;
    }

    public void setWinnerPrice(Double winnerPrice) {
        this.winnerPrice = winnerPrice;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getStatusComment() {
        return statusComment;
    }

    public void setStatusComment(String statusComment) {
        this.statusComment = statusComment;
    }

    public String getPurchID() {
        return purchID;
    }

    public void setPurchID(String purchID) {
        this.purchID = purchID;
    }

    public String getAsid() {
        return asid;
    }

    public void setAsid(String asid) {
        this.asid = asid;
    }

    public String getPurchName() {
        return purchName;
    }

    public void setPurchName(String purchName) {
        this.purchName = purchName;
    }

    public String getReqID() {
        return reqID;
    }

    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public Double getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(Double maxprice) {
        this.maxprice = maxprice;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getPurchCode() {
        return purchCode;
    }

    public void setPurchCode(String purchCode) {
        this.purchCode = purchCode;
    }

    public Algoritm getIdAlg() {
        return idAlg;
    }

    public void setIdAlg(Algoritm idAlg) {
        this.idAlg = idAlg;
    }

    public Providers getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(Providers idProvider) {
        this.idProvider = idProvider;
    }

    public Stakestatus getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Stakestatus idStatus) {
        this.idStatus = idStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stakelist)) {
            return false;
        }
        Stakelist other = (Stakelist) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pockermen.database.Stakelist[ id=" + id + " ]";
    }
    
}
