/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.database;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Evgeniy
 */
@Entity
@Table(name = "stakestatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stakestatus.findAll", query = "SELECT s FROM Stakestatus s")
    , @NamedQuery(name = "Stakestatus.findById", query = "SELECT s FROM Stakestatus s WHERE s.id = :id")
    , @NamedQuery(name = "Stakestatus.findBySname", query = "SELECT s FROM Stakestatus s WHERE s.sname = :sname")})
public class Stakestatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "sname")
    private String sname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idStatus")
    private Collection<Stakelist> stakelistCollection;

    public Stakestatus() {
    }

    public Stakestatus(Integer id) {
        this.id = id;
    }

    public Stakestatus(Integer id, String sname) {
        this.id = id;
        this.sname = sname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @XmlTransient
    public Collection<Stakelist> getStakelistCollection() {
        return stakelistCollection;
    }

    public void setStakelistCollection(Collection<Stakelist> stakelistCollection) {
        this.stakelistCollection = stakelistCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stakestatus)) {
            return false;
        }
        Stakestatus other = (Stakestatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pockermen.database.Stakestatus[ id=" + id + " ]";
    }
    
}
