/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import java.util.List;
import pockermen.database.Stakestatus;

/**
 *
 * @author Evgeniy
 */
public class StatusStake {
    public static enum EStatus{
        isEnter,//Введен номер
        isError,//Ошибка ставки
        isLoadingParm,//Идет запрос параметров
        isGetParm,//Запрошены параметры
        isRunTimer,//Задан таймер
        isAction,//Идут торги
        isWiteResult,//Ожидание результатов
        isWinner,//Победитель
        isLoss,//Проигрышь
        isCancel//Не состоялись
    }
    
    private static List<Stakestatus> statusList;
    
    public static void loadStatusList(){
        statusList = Singleton.getInstance().getSession().createQuery("from Stakestatus").list();
    }
    
    public static EStatus castIdToEStatus(int id){
        EStatus state;
        switch(id){
            case 10:
                state = EStatus.isEnter;
                break;
            case 20:
                state = EStatus.isError;
                break;
            case 25:
                state = EStatus.isLoadingParm;
                break;
            case 30:
                state = EStatus.isGetParm;
                break;
            case 35:
                state = EStatus.isRunTimer;
                break;
            case 40:
                state = EStatus.isAction;
                break;
            case 50:
                state = EStatus.isWiteResult;
                break;
            case 60:
                state = EStatus.isWinner;
                break;
            case 70:
                state = EStatus.isLoss;
                break;
            case 80:
                state = EStatus.isCancel;
                break;
            default:
                throw new UnsupportedOperationException("Not supported stake status."); //To change body of generated methods, choose Tools | Templates.
        }
        return state;
    }
    
    public static int castEStatusToId(EStatus state){
        int id;
        switch (state) {
            case isEnter:
                id = 10;
                break;
            case isError:
                id = 20;
                break;
            case isLoadingParm:
                id = 25;
                break;
            case isGetParm:
                id = 30;
                break;
            case isRunTimer:
                id = 35;
                break;
            case isAction:
                id = 40;
                break;
            case isWiteResult:
                id = 50;
                break;
            case isWinner:
                id = 60;
                break;
            case isLoss:
                id = 70;
                break;
            case isCancel:
                id = 80;
                break;
            default:
                throw new UnsupportedOperationException("Not supported stake status.");
        }
        
        return id;
    }
    
    public static Stakestatus getStakestatusFromEStatus(EStatus status){
        int id = castEStatusToId(status);
        for(Stakestatus ss : statusList){
            if(ss.getId() == id)
                return ss;
        }
        return null;
    }
    
}
