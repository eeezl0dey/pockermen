/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import java.io.InputStream;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.LibraryLoader;
import com.jacob.com.Variant;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;

/**
 *
 * @author user
 */
public class Pockermen extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLMain.fxml"));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLMain.fxml"));
        Parent root = (Parent) loader.load();

        Scene scene = new Scene(root);
        
        stage.setOnCloseRequest(e -> {
            Singleton.getInstance().stopProviders();
            Singleton.getInstance().sessionClose();
            Platform.exit();
//                System.exit(0);
        });        

        FXMLMainController controller = (FXMLMainController) loader.getController();
        controller.setStage(stage);
        Singleton.getInstance().setFormController(controller);
        stage.setScene(scene);
        stage.setTitle("Pockermen v1.01");
        stage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        Singleton.getInstance();
    }
    
}

