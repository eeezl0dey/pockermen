/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import pockermen.alg.AlgStriker;
import pockermen.alg.BaseAlg;
import pockermen.database.Stakelist;
import pockermen.html.BaseHTMLparser;

/**
 *
 * @author Evgeniy
 */
public class Provider {

    private Long difftime;// Разница времени с сервером по относительно локальной машины
    private final BaseHTMLparser parserThread;
    private Timer timerAlg;//Таймер обработки милисекунд
    private String provName;//Наименование провайдера
    private ParserFactory<BaseHTMLparser> parserFactory;

    public interface ParserFactory<P extends BaseHTMLparser> {

        P create();
    }

    //public Provider(BaseHTMLparser aparserThread){
    public Provider(ParserFactory<BaseHTMLparser> apf) {
        parserFactory = apf;
        parserThread = parserFactory.create();
        provName = parserThread.getClass().getName();
        parserThread.setName(provName);
        timerAlg = new Timer("TimerProv_" + provName);
        parserThread.setProvider(this);
        parserThread.addAction(BaseHTMLparser.Action.LoadTime, new Vector<>());
        Vector<Object> vobj = new Vector<>();
        vobj.add(this);

//        parserThread.addAction(BaseHTMLparser.Action.Finish, new Vector<>());
//        Vector<Object> tobj = new Vector<>();
//        tobj.add(new Tocken("AC1DE0843B0B33502CA21A076748568524D5BA33", "Зайцев Алексей Александрович; (до 22.03.2018 08:00:16)"));
//        parserThread.addAction(BaseHTMLparser.Action.Login, tobj);
//        tmsec.schedule(msTimerTask , 100000, 100000);
    }

    public void startThread() {
        parserThread.start();
    }

    public void stopThread() {
        if (parserThread.isAlive()) {
            parserThread.finish();
        }
        getTimerAlg().cancel();
        getTimerAlg().purge();
    }

    @Override
    protected void finalize() throws Throwable {
        stopThread();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getProvName() {
        return parserThread.getProviderName();
    }

    //Запуск действий по ставкам в зависимости от из статуса
    public void processingStake(Stakelist stake) {
        Platform.runLater(() -> {
            StatusStake.EStatus status = StatusStake.castIdToEStatus(stake.getIdStatus().getId());
            Vector<Object> tobj;
            switch (status) {
                case isEnter://Нововведеная ставка, необходимо проверить наличие и запросить параметры
                    tobj = new Vector<>();
                    tobj.add(stake);
                    parserThread.addAction(BaseHTMLparser.Action.CheckStake, tobj);
                    break;
                case isGetParm://Параметры есть, необходим таймер на запуск алгоритма
                    tobj = new Vector<>();
                    tobj.add(stake);
                    parserThread.addAction(BaseHTMLparser.Action.PreStartStake, tobj);                                       
                    break;
            }
            Singleton.getInstance().getFormController().refreshData();
        });

    }

    /**
     * @return the difftime
     */
    public synchronized Long getDifftime() {
        return difftime;
    }

    /**
     * @param difftime the difftime to set
     */
    public synchronized void setDifftime(Long difftime) {
        this.difftime = difftime;
    }

    public synchronized void processingStakeAll() {
        ObservableList<Stakelist> stakeList = Singleton.getInstance().getStakeList();
        for (Stakelist curstake : stakeList) {
            if (parserThread.getProv() == Singleton.idToProv(curstake.getIdProvider().getId())) {
                processingStake(curstake);
            }
        }

    }

    public void addActionProcessing() {
        parserThread.addAction(BaseHTMLparser.Action.Processing, new Vector<>());
    }
    
     public void addActionLoadTime() {
        parserThread.addAction(BaseHTMLparser.Action.LoadTime, new Vector<>());
    }
     
    public void addActionCheckStakeDetail(Stakelist stake) {
        Vector<Object> tobj = new Vector<>();
        tobj.add(stake);
        parserThread.addAction(BaseHTMLparser.Action.CheckStakeDetail, tobj);
    }

    public void addActionSetTimerAlg(Stakelist stake) {
        if(StatusStake.castIdToEStatus(stake.getIdStatus().getId()) == StatusStake.EStatus.isRunTimer){
            Vector<Object> tobj = new Vector<>();
            tobj.add(stake);
            parserThread.addAction(BaseHTMLparser.Action.SetTimerAlg, tobj);
        }
    }
    
    /**
     * @return the parserFactory
     */
    public ParserFactory<BaseHTMLparser> getParserFactory() {
        return parserFactory;
    }

    /**
     * @return the timerAlg
     */
    public Timer getTimerAlg() {
        return timerAlg;
    }

}
