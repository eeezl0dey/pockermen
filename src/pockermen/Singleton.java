/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Session;
import pockermen.alg.BaseAlg;
import pockermen.database.HibernateUtil;
import pockermen.database.Stakelist;
import pockermen.html.SberHTMLparser;

/**
 *
 * @author user
 */
public class Singleton {

    public enum Prov {
        Sberbank
    };

    private static volatile Singleton instance;
    private Session session = null;
    private Map<Prov, Provider> providers; //для каждого провайдера по одному постоянному потоку обработки
    private Map<Stakelist, BaseAlg> timerStake;
    private FXMLMainController formController;
    private ObservableList<Stakelist> stakeList;
    private List<Tocken> listTocken = null;
    
    public static Singleton getInstance() {
        Singleton localInstance = instance;
        if (localInstance == null) {
            synchronized (Singleton.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Singleton();
                }
            }
        }
        return localInstance;
    }

    public Singleton() {
        session = HibernateUtil.getSessionFactory().openSession();
        stakeList = null;
        listTocken = Tocken.getListTocken();
        initProviders();
    }

    @Override
    protected void finalize() throws Throwable {
        sessionClose();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the session
     */
    public Session getSession() {
        return session;
    }

    public void sessionClose() {
        if (session != null) {
            session.close();
        }
    }

    // загрузка провайдеров
    public int initProviders() {
        timerStake = new HashMap<>();
        providers = new HashMap<>();
        Provider provSber = new Provider(SberHTMLparser::new);
        provSber.addActionProcessing();
        provSber.startThread();
        getProviders().put(Prov.Sberbank, provSber);
        return 1;
    }

    /**
     * @return the formController
     */
    public FXMLMainController getFormController() {
        return formController;
    }

    /**
     * @param formController the formController to set
     */
    public synchronized void setFormController(FXMLMainController formController) {
        this.formController = formController;
    }

    // остановка провайдеров
    public int stopProviders() {
        for (BaseAlg baseAlg : timerStake.values()) {
            baseAlg.stopThread();
        }

        Collection<Provider> colProv = providers.values();
        for (Provider pr : colProv) {
            pr.stopThread();
        }
        return 1;
    }

    public static int provToId(Prov prov) {
        switch (prov) {
            case Sberbank:
                return 1;
        }
        return 0;
    }

    public static Prov idToProv(int id) {
        switch (id) {
            case 1:
                return Prov.Sberbank;
        }
        throw new UnsupportedOperationException("Неопределенный тип провайдера");
    }

    public Provider getProviderById(int id) {
        Provider provider = providers.get(Singleton.idToProv(id));
        return provider;
    }

    /**
     * @return the providers
     */
    public Map<Prov, Provider> getProviders() {
        return providers;
    }

    /**
     * @return the timerStake
     */
    public Map<Stakelist, BaseAlg> getTimerStake() {
        return timerStake;
    }

    /**
     * @return the stakeList
     */
    public ObservableList<Stakelist> getStakeList() {
        if (stakeList == null) {
            stakeList = FXCollections.observableList(session.createQuery("from Stakelist").list());
        }
        return stakeList;
    }

    /**
     * @return the listTocken
     */
    public List<Tocken> getListTocken() {
        return listTocken;
    }


}
