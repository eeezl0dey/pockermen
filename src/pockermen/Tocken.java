/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pockermen.html.BaseHTMLparser;
import pockermen.html.SberHTMLparser;

/**
 *
 * @author Evgeniy
 */
public class Tocken{
    private String name;
    private Date dateFrom;
    private Date dateTo;
    private Dispatch certificate;
    private String thumbprint;
    
    public static class Capicom_store_location {

        static int CAPICOM_MEMORY_STORE = 0; //The store is a memory store. Any changes in the contents of the store are not persisted.
        static int CAPICOM_LOCAL_MACHINE_STORE = 1; //The store is a local machine store. Local machine stores can be read/write stores only if the user has read/write permissions. If the user has read/write permissions and if the store is opened read/write, then changes in the contents of the store are persisted.
        static int CAPICOM_CURRENT_USER_STORE = 2; //The store is a current user store. A current user store may be a read/write store. If it is, changes in the contents of the store are persisted.
        static int CAPICOM_ACTIVE_DIRECTORY_USER_STORE = 3; //The store is an Active Directory store. Active Directory stores can be opened only in read-only mode. Certificates cannot be added to or removed from Active Directory stores.
        static int CAPICOM_SMART_CARD_USER_STORE = 4; //Stores support smart card–based certificate stores. The store is the group of present smart cards. Introduced in CAPICOM 2.0.
    };

    public static class Capicom_store_open_mode {

        static int CAPICOM_STORE_OPEN_READ_ONLY = 0; //Open the store in read-only mode.
        static int CAPICOM_STORE_OPEN_READ_WRITE = 1; //Open the store in read/write mode.
        static int CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED = 2; //Open the store in read/write mode if the user has read/write permissions. If the user does not have read/write permissions, open the store in read-only mode.
        static int CAPICOM_STORE_OPEN_EXISTING_ONLY = 128; //Open existing stores only; do not create a new store. Introduced by CAPICOM 2.0.
        static int CAPICOM_STORE_OPEN_INCLUDE_ARCHIVED = 256; //Include archived certificates when using the store. Introduced by CAPICOM 2.0.
    }
    
    public Tocken(Dispatch aCertificate){
            certificate = aCertificate;
            name = Dispatch.call(certificate, "GetInfo", new Variant(0)).getString();
            dateFrom = Dispatch.get(certificate, "ValidFromDate").getJavaDate();
            dateTo = Dispatch.get(certificate, "ValidToDate").getJavaDate();
            thumbprint = Dispatch.get(certificate, "Thumbprint").getString();
    }

      
    @Override
    public String toString()  {
        //SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        DateFormat dfDate = new SimpleDateFormat("dd.MM.yyyy");
        Date dtime = null;
        String sDateFrom = dfDate.format(getDateFrom());
        String sDateTo = dfDate.format(getDateTo());
        return getName() + " (" +sDateFrom+ " - " + sDateTo + ")";
    }
    
    //Загрузка списка токенов
    static public List<Tocken> getListTocken() {
        List<Tocken> listTocken = new ArrayList<>();
//        Singleton.getInstance().getFormController().setStatus("Запрос списка токенов");
        //ActiveXComponent signer = new ActiveXComponent("clsid:{91D221C4-0CD4-461C-A728-01D509321556}");
        ActiveXComponent signer = new ActiveXComponent("CAPICOM.Store");
        Variant argLocation = new Variant(Capicom_store_location.CAPICOM_CURRENT_USER_STORE);
        Variant argStore = new Variant("My");
        Variant argMode = new Variant(Capicom_store_open_mode.CAPICOM_STORE_OPEN_READ_ONLY);
        signer.invoke("Open",
                argLocation,
                argStore,
                argMode);
        Dispatch dSigners;
        dSigners = signer.getProperty("Certificates").getDispatch();
        int iCount = Dispatch.get(dSigners, "Count").getInt();

        for (int i = 0; i < iCount; i++) {
            Dispatch dCertificate = Dispatch.call(dSigners, "Item", new Variant(i + 1)).toDispatch();
            Tocken tocken = new Tocken(dCertificate);
//            Variant vName = Dispatch.call(dSign, "GetInfo", new Variant(0));
//            String sName = Dispatch.call(dSign, "GetInfo", new Variant(0)).getString();
//            Date dFrom = Dispatch.get(dSign, "ValidFromDate").getJavaDate();
//            Date dTo = Dispatch.get(dSign, "ValidToDate").getJavaDate();
//            Singleton.getInstance().getFormController().setStatus("Загружен сертификат: " + tocken);
            listTocken.add(tocken);
        }


//        Singleton.getInstance().getFormController().setStatus("Список окончен");
        return listTocken;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the dateFrom
     */
    public Date getDateFrom() {
        return dateFrom;
    }

    /**
     * @return the dateTo
     */
    public Date getDateTo() {
        return dateTo;
    }

    /**
     * @return the sign
     */
    public Dispatch getCertificate() {
        return certificate;
    }

    /**
     * @return the Thumbprint
     */
    public String getThumbprint() {
        return thumbprint;
    }

}
