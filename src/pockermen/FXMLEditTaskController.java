/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.hibernate.Session;
import pockermen.Provider;
import pockermen.Singleton;
import pockermen.StatusStake;
import pockermen.Tocken;
import pockermen.alg.BaseAlg;
import pockermen.database.Algoritm;
import pockermen.database.Providers;
import pockermen.database.Stakelist;
import pockermen.database.Stakestatus;

/**
 * FXML Controller class
 *
 * @author user
 */
public class FXMLEditTaskController implements Initializable {

    private Stage stage;
    private Stakelist stake;
    private List<Providers> providersList;
    private List<Algoritm> algoritmList;
    
    DateFormat dfDate = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

    @FXML
    private Button bAccept;

    @FXML
    private Button bCancel;

    @FXML
    private ComboBox<String> cbProvider;
    @FXML
    private ComboBox<Tocken> cbTocken;
    @FXML
    private TextField tStake;
    @FXML
    private ComboBox<String> cbAlgoritm;
    @FXML
    private TextField tPrice;

//    @FXML
//    private ComboBox<String> cbTocken;
//    
//    @FXML
//    private TextField tStake;
//    
//    @FXML
//    private ComboBox<String> cbAlgoritm;
//    
//    @FXML
//    private TextField tPrice;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        providersList = Singleton.getInstance().getSession().createQuery("from Providers").list();
        algoritmList = Singleton.getInstance().getSession().createQuery("from Algoritm").list();
        //Collection<Provider> coll_pr = Singleton.getInstance().getProviders().values();
//        cbProvider.setCellFactory(value);
        for (Providers pr : providersList) {
            cbProvider.getItems().add(pr.getPname());
        }

        Callback cellFactory = new Callback<ListView<Tocken>, ListCell<Tocken>>() {
            @Override
            public ListCell<Tocken> call(ListView<Tocken> l) {
                return new ListCell<Tocken>() {
                    @Override
                    protected void updateItem(Tocken item, boolean empty) {
                        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.toString());
                        }
                    }
                };
            }
        };
        cbTocken.setCellFactory(cellFactory);
        cbTocken.setDisable(true);

        cbAlgoritm.getItems().clear();
        int algCount = 0;
        for (Algoritm alg : algoritmList) {
            cbAlgoritm.getItems().add(alg.getAlgname());
            algCount++;
        }
        if (algCount > 0) {
            cbAlgoritm.getSelectionModel().select(0);
        }

        //Форматтер для вещественных
        UnaryOperator<TextFormatter.Change> filterDecimal = new UnaryOperator<TextFormatter.Change>() {
            @Override
            public TextFormatter.Change apply(TextFormatter.Change t) {

                if (t.isReplaced()) {
                    if (t.getText().matches("[^0-9]")) {
                        t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                    }
                }

                if (t.isAdded()) {
                    if (t.getControlText().contains(".")) {
                        if (t.getText().matches("[^0-9]")) {
                            t.setText("");
                        }
                    } else if (t.getText().matches("[^0-9.]")) {
                        t.setText("");
                    }
                }

                return t;
            }
        };

        //Установим для прайса форматтер
        tPrice.setTextFormatter(new TextFormatter<>(filterDecimal));
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void handleAccept(ActionEvent event) throws IOException {
        System.out.println("You clicked handleAccept!");
        Session session = Singleton.getInstance().getSession();
        try {
            setStakeFromForm();
            session.beginTransaction();
            if (stake.getId() != 0) {
                //Если запущен таймер то надо остановить
                Map<Stakelist, BaseAlg> mtimers = Singleton.getInstance().getTimerStake();
                for (Stakelist stakelist : mtimers.keySet()) {
                    if (stakelist.getId() == stake.getId()) {
                        BaseAlg alg = mtimers.get(stakelist);
                        alg.cancel();

                    }
                }
                session.update(stake);
            } else {
                session.save(stake);
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            Singleton.getInstance().getFormController().setStatus("Ошибка сохранения: " + e.getMessage());
        };
        Singleton.getInstance().getFormController().refreshData();
        Provider prov = Singleton.getInstance().getProviderById(stake.getIdProvider().getId());
        prov.processingStake(stake);
        stage.close();
    }

    @FXML
    private void handleCancel(ActionEvent event) throws IOException {
        System.out.println("You clicked handleCancel!");
        stage.close();
    }

    @FXML
    private void handleSelectProvider(ActionEvent event) throws IOException {
        System.out.println("handleSelectProvider!");
        loadTockenFromProvider();
        if (cbTocken.getItems().size() > 0) {
            cbTocken.getSelectionModel().select(0);
        }

    }

    private void loadTockenFromProvider() {
        Collection<Provider> coll_pr = Singleton.getInstance().getProviders().values();
        for (Provider pr : coll_pr) {
            System.out.println(cbProvider.getValue());
            if (pr.getProvName().equals(cbProvider.getValue())) {
                cbTocken.setDisable(false);
                Collection<Tocken> listTocken = Singleton.getInstance().getListTocken();
                cbTocken.getItems().clear();
                cbTocken.getItems().addAll(listTocken);
            }
        }
    }

    @FXML
    private void handleSelectTocken(ActionEvent event) throws IOException {
        System.out.println("handleSelectTocken!");
    }

    @FXML
    private void handleSelectAlgoritm(ActionEvent event) throws IOException {
        System.out.println("handleSelectAlgoritm!");
    }

    /**
     * @return the stake
     */
    public Stakelist getStake() {
        return stake;
    }

    /**
     * @param stake the stake to set
     */
    public void seFormFromtStake(Stakelist stake) {
        this.stake = stake;
        if (stake.getId() != 0) {
            //Выставим провайдера
            int index = 0;
            int id = stake.getIdProvider().getId();
            for (Providers pr : providersList) {
                if (id == pr.getId()) {
                    cbProvider.getSelectionModel().select(index);
                    break;
                }
                index++;
            }
            //Выставим токен
            index = 0;
            loadTockenFromProvider();
            List<Tocken> listTocken = cbTocken.getItems();
            String tocken = stake.getEcp();
            for (Tocken curTocken : listTocken) {
                String sDateTo = dfDate.format(curTocken.getDateTo());
                 String curTockenName = curTocken.getName() + "; (до " + sDateTo + ")";
                if (tocken.equals(curTockenName)) {
                    cbTocken.getSelectionModel().select(index);
                    cbTocken.setDisable(false);
                    break;
                }
                index++;
            }
            //Номер ставки
            tStake.setText(stake.getAuction());
            //Алгоритм бота
            index = 0;
            id = stake.getIdAlg().getId();
            for (Algoritm alg : algoritmList) {
                if (id == alg.getId()) {
                    cbAlgoritm.getSelectionModel().select(index);
                    break;
                }
                index++;
            }
            //Цена
            tPrice.setText(String.valueOf(stake.getPrice()));
        }
    }

    //Заполнение данных из формы
    private void setStakeFromForm() {
        //Выставим провайдера
        int index = cbProvider.getSelectionModel().getSelectedIndex();
        stake.setIdProvider(providersList.get(index));

        //Выставим токен
        index = 0;
        Tocken tocken = cbTocken.getSelectionModel().getSelectedItem();
        String sDateTo = dfDate.format(tocken.getDateTo());
        stake.setEcp(tocken.getName() + "; (до " + sDateTo + ")");
        //Зайцев Алексей Александрович; (до 19.06.2019 12:23:01
        //Номер ставки
        stake.setAuction(tStake.getText());
        //Алгоритм бота
        index = cbAlgoritm.getSelectionModel().getSelectedIndex();
        stake.setIdAlg(algoritmList.get(index));
        //Цена
        stake.setPrice(Double.parseDouble(tPrice.getText()));
        //Сбросим статус
        stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isEnter));
        stake.setDateStart(null);
        stake.setContractor(null);
        stake.setMaxprice(null);
        stake.setDateStart(null);
        stake.setDateEnd(null);
        stake.setWinnerPrice(null);
        stake.setPurchID(null);
        stake.setPurchName(null);

        if (stake.getId() == 0) {
            Singleton.getInstance().getFormController().addStake(stake);
        }
    }

}
