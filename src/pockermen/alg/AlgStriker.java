/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.alg;

import pockermen.Provider;
import pockermen.StatusStake;
import pockermen.database.Stakelist;
import pockermen.html.BaseHTMLparser;

/**
 *
 * @author Evgeniy
 */
public class AlgStriker extends BaseAlg {

    public AlgStriker(Stakelist astake, Provider.ParserFactory<BaseHTMLparser> apf) {
        super(astake, apf);
    }

    @Override
    public String getAlgName() {
        return "Страйкер";
    }

    @Override
    public int getId() {
        return 1;
    }

    //Заблаговременный запуск по таймеру
    @Override
    public void run() {
        super.run();
        //Собираем инфу для подготовки самих JSON запросов
//        provider.addActionLogin();
        provider.addActionCheckStakeDetail(stake);
        //Запускаем таймер за старт
        provider.addActionSetTimerAlg(stake);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
