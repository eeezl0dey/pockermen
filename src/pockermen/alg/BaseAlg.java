/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.alg;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import pockermen.Provider;
import pockermen.Singleton;
import pockermen.Singleton.Prov;
import pockermen.database.Stakelist;
import pockermen.html.BaseHTMLparser;
import pockermen.html.SberHTMLparser;

/**
 *
 * @author Evgeniy
 */
public abstract class BaseAlg extends TimerTask {

    protected Provider provider;
    protected Stakelist stake;
    //protected Long timeDifferent;//Отличие времени от локальной машины
    protected Provider.ParserFactory<BaseHTMLparser> parserFactory; //Фабрика для создания парсера

    public abstract int getId();

    public abstract String getAlgName();

    public BaseAlg(Stakelist astake, Provider.ParserFactory<BaseHTMLparser> apf) {
        stake = astake;
        parserFactory = apf;
        provider = new Provider(parserFactory);
    }
    
    public void stopThread() {
        provider.stopThread();
    }


    @Override
    public String toString() {
        return getAlgName();
    }

    @Override
    public void run() {
        provider.startThread();
    }

}
