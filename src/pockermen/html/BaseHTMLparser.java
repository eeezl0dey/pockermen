/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.html;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.net.ssl.SSLContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import pockermen.Provider;
import pockermen.Singleton;
import pockermen.Singleton.Prov;
import pockermen.StatusStake;
import pockermen.Tocken;
import pockermen.alg.AlgStriker;
import pockermen.alg.BaseAlg;
import pockermen.database.Stakelist;

/**
 *
 * @author Evgeniy
 */
public abstract class BaseHTMLparser extends Thread {
    
    protected Logger logger;
    protected final String LOG_PATH = "Logs.log";

    public enum Action {
        LoadTime, //Загрузка времени сервера
        Finish, //Завершение процесса
        CheckStake, //Проверка ставки
        Login, //Залогиниться на сайте
        Logout, //Выйти с сайта
        PreStartStake, //Запускает ставку за 5 мин до аукциона
        Processing, //Вызов внешней обработки (функции синглтона для постановки в очередь)
        CheckStakeDetail, //Проверка ставки и параметров аукциона
        SetTimerAlg //Установка таймера запуска алгоритма
    };

    class TaskParser {

        private Action action;
        private Vector<Object> parm;

        public TaskParser(Action action) {
            this.action = action;
            parm = new Vector<>();
        }

        public TaskParser(Action action, Vector<Object> parm) {
            this.action = action;
            this.parm = parm;
        }

        /**
         * @return the action
         */
        public Action getAction() {
            return action;
        }

        /**
         * @return the parm
         */
        public Vector<Object> getParm() {
            return parm;
        }
    }

    protected Prov prov;
    protected String ProviderName;//Наименование провайдера
    private volatile boolean mFinish = false;//Признак к зкрытию потока
    protected Queue<TaskParser> queueAction;
    protected Provider provider;
    protected CookieStore cookies;
//    protected CookieStore cookies12;

    public BaseHTMLparser() {
        try {
            FileHandler logFileHandler;
            logFileHandler = new FileHandler(LOG_PATH);
            SimpleFormatter sf = new SimpleFormatter();
            logFileHandler.setFormatter(sf);
            logger = Logger.getLogger(BaseHTMLparser.class.getName());
            logger.addHandler(logFileHandler);
        } catch (IOException ex) {
            Logger.getLogger(BaseHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(BaseHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
        }


        queueAction = new LinkedList<>();
        ProviderName = "Не заполнен";
        provider = null;
        cookies = new BasicCookieStore();

        BasicClientCookie ym_uid = new BasicClientCookie("_ym_uid", "1520744110530514036");
        ym_uid.setDomain("login.sberbank-ast.ru");
        ym_uid.setPath("/");
        cookies.addCookie(ym_uid);
        BasicClientCookie ym_isad = new BasicClientCookie("_ym_isad", "2");
        ym_isad.setDomain("login.sberbank-ast.ru");
        ym_isad.setPath("/");
        cookies.addCookie(ym_isad);
        BasicClientCookie ga = new BasicClientCookie("_ga", "GA1.2.1256571250.1520744199");
        ga.setDomain("login.sberbank-ast.ru");
        ga.setPath("/");
        cookies.addCookie(ga);
        BasicClientCookie gid = new BasicClientCookie("_gid", "GA1.2.72438400.1520744199");
        gid.setDomain("login.sberbank-ast.ru");
        gid.setPath("/");
        cookies.addCookie(gid);

    }

    public abstract void loadTime();

    public abstract void login(Tocken tocken);

    public abstract void logout();

    public abstract void logout(CloseableHttpClient httpClient);

    //Запуск по таймеру алгоритма ставок
    public abstract void timerStake(Stakelist stake);

    //Инициирует завершение потока
    public void finish() {
        Singleton.getInstance().getFormController().setStatus("Завершение процесса");
        mFinish = true;
    }

    public synchronized void addAction(Action action, Vector<Object> parm) {
        queueAction.add(new TaskParser(action, parm));
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        boolean hiddenBrowser = true;

        Tocken tocken;
        Stakelist stake;

        do {
            if (!queueAction.isEmpty()) {
                TaskParser tp = queueAction.poll();
                switch (tp.getAction()) {
                    case LoadTime:
                        loadTime();
                        break;
                    case Finish:
                        finish();
                        break;
                    case CheckStake:
                        stake = (Stakelist) tp.parm.elementAt(0);
                        tocken = findTocken(stake.getEcp());
                        if (tocken != null) {
                            login(tocken);
                            checkStake(stake, false);
                            Singleton.getInstance().getFormController().refreshData();
                        } else {
                            Singleton.getInstance().getFormController().setStatus("Не найден токен: " + stake.getEcp());
                        }
                        break;
                    case CheckStakeDetail:
                        stake = (Stakelist) tp.parm.elementAt(0);
                        tocken = findTocken(stake.getEcp());
                        if (tocken != null) {
                            login(tocken);
                            checkStake(stake, true);
                            Singleton.getInstance().getFormController().refreshData();
                        } else {
                            Singleton.getInstance().getFormController().setStatus("Не найден токен: " + stake.getEcp());
                        }
                        break;
                    case PreStartStake:
                        Stakelist prestartstake = (Stakelist) tp.parm.elementAt(0);
                        preStartStake(prestartstake);
                        break;
                    case Login:
                        tocken = (Tocken) tp.parm.elementAt(0);
                        login(tocken);
                        break;
                    case Logout:
                        logout();
                        break;
                    case Processing:
                        if (provider != null) {
                            provider.processingStakeAll();
                        } else {
                            throw new NullPointerException("Провайдер не привязан к парсеру");
                        }
                        break;
                    case SetTimerAlg:
                        Stakelist algstake = (Stakelist) tp.parm.elementAt(0);
                        timerStake(algstake);
                        break;
                }

            }
            Thread.yield();
        } while (!mFinish);

    }

    /**
     * @return the ProviderName
     */
    public synchronized String getProviderName() {
        return ProviderName;
    }

    //Заходит на ставку, проверяет наличие и запрашивает ее параметры
    protected abstract void checkStake(Stakelist stake, boolean isDetail);

    //Поиск токена среди списка по его описанию (ФИО)
    public Tocken findTocken(String aName) {
        List<Tocken> listTocken = Singleton.getInstance().getListTocken();
        DateFormat dfDate = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        for (Tocken tocken : listTocken) {
            String sDateTo = dfDate.format(tocken.getDateTo());
            String curTockenName = tocken.getName() + "; (до " + sDateTo + ")";

            if (aName.equals(curTockenName)) {
                return tocken;
            }
        }
        return null;
    }

    /**
     * @return the provider
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    /**
     * @return the prov
     */
    public Prov getProv() {
        return prov;
    }

    public void preStartStake(Stakelist stake) {
        BaseAlg alg = null;
        Date curTime = new Date();
        Long timeStart = 0L;
        Date stime = null;
        switch (stake.getIdAlg().getId()) {
            case 1:
                alg = new AlgStriker(stake, provider.getParserFactory());
                //Запускаем страйкер за .. мин до торгов
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy hh:mm");
                try {
                    //Переведем дату в документе в формат
                    String startDate = stake.getDateStart();
                    System.out.println("processingStake парсинг даты:" + startDate);
                    stime = format.parse(startDate);
                } catch (ParseException ex) {
                    logger.log(Level.SEVERE, null, ex);
                    String sErr = "Ошибка формата времени при постановке таймера";
                    Singleton.getInstance().getFormController().setStatus("Аукцион: " + stake.getAuction() + " - " + sErr);
                    stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
                    stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":" + sErr);
                    Singleton.getInstance().getFormController().refreshData();
                    return;
                }
                //Прибавим разницу с нашей машиной и вычтем время для заблаговременного старта
                int startBeforeMin = 5;//за ... минут
                timeStart = stime.getTime() + provider.getDifftime() - startBeforeMin * 60 * 1000;

        }

        if (timeStart > 0L) {
            //Если прошло 10 минут после аукциона то проверим выигран ли он и на какая сумма сиграла
            int diffTimeFinish = 10; //В минутах
            long limitTime = new Date().getTime() + diffTimeFinish * 60 * 1000;
//            if (limitTime > stime.getTime()) {
            //TODO Здесь можно узнать о результате аукциона
//            } else {
            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isRunTimer));
            Date startTime;
            startTime = new Date(timeStart);
            Timer timerAlg = provider.getTimerAlg();
            timerAlg.schedule(alg, startTime);
            Map<Stakelist, BaseAlg> mtimer = Singleton.getInstance().getTimerStake();
            mtimer.put(stake, alg);

//            }
        }

    }

    public String convertStreamToString(java.io.InputStreamReader is) {
        String result;
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        result = s.hasNext() ? s.next() : "";
        Charset.forName("UTF-8").encode(result);
        return result;
    }

    public String httpResponseContent(HttpResponse response) {
        String result = null;
//        int stCode = response.getStatusLine().getStatusCode();
//        try {
//            if (stCode != 200) {
//                String serr = "Error " + stCode + " when accessing URL ";
//                Singleton.getInstance().getFormController().setStatus(serr);
//            } else {
//                HttpEntity entity = response.getEntity();
//                InputStreamReader isr;
//                isr = new InputStreamReader(entity.getContent(), "UTF-8");
//                result = convertStreamToString(isr);
//                isr.close();
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(BaseHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
//            Singleton.getInstance().getFormController().setStatus("Ошибка чтения контента:" + ex.toString());
//        }
//        return result;
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            InputStream is = null;
            try {
                is = entity.getContent();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            } catch (UnsupportedOperationException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            result = convertStreamToString(is);
        }
        return result;
    }

    public String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public HttpResponse getUrl(CloseableHttpClient httpClient, String aUrl) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        return getUrl(httpClient, aUrl, null);
    }

    public HttpResponse getUrl(CloseableHttpClient httpClient, String aUrl, List<NameValuePair> params) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpResponse response = null;
        try {

            HttpGet getRequest = null;

            //httpClient = HttpClients.createDefault();
            if (params == null) {
                getRequest = new HttpGet(aUrl);
            } else {
                getRequest = new HttpGet(aUrl + "?" + URLEncodedUtils.format(params, "utf-8"));
            }

            getRequest.addHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            getRequest.addHeader("Content-Type", "text/plain; charset=utf-8");
            getRequest.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36");

            response = httpClient.execute(getRequest);

            System.out.println("Cookie : ");
            for (Cookie cok : cookies.getCookies()) {
                System.out.println(cok.getName() + ":" + cok.getValue());
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка запроса к странице: " + ex.toString());
        }

        return response;
    }

//Метод для считывания ответа сервера из входного потока + преобразование в строку
    private String inputStreamToString(InputStream is) {
        String s = "";
        String line = "";
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((line = rd.readLine()) != null) {
                s += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    /*
    *************************************************************************
     */
    public HttpResponse postUrl(CloseableHttpClient httpClient, String aUrl, List<NameValuePair> params) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpResponse response = null;

        try {
            HttpPost postRequest;

            URI urlObject = null;
            try {
                urlObject = new URI(aUrl);
            } catch (URISyntaxException ex) {
                logger.log(Level.SEVERE, null, ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка URL:" + ex);
            }

            postRequest = new HttpPost(aUrl);

//            int timeoutSeconds = 60;
//            int CONNECTION_TIMEOUT_MS = timeoutSeconds * 1000; // Timeout in millis.
//            RequestConfig requestConfig = RequestConfig.custom()
//                    .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
//                    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
//                    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
//                    .build();
//            postRequest.setConfig(requestConfig);
//            postRequest.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
//            postRequest.addHeader("Accept-Encoding", "gzip, deflate, br");
            postRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
//            postRequest.addHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            postRequest.addHeader("Connection", "keep-alive");
            postRequest.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36");
//            postRequest.addHeader("Origin", "https://login.sberbank-ast.ru");
            postRequest.addHeader("Upgrade-Insecure-Requests", "1");
//            postRequest.addHeader("Cache-Control", "max-age=0");
//            postRequest.addHeader("Referer", "https://login.sberbank-ast.ru/Login.aspx?ReturnUrl=%2f%3fwa%3dwsignin1.0%26wtrealm%3dhttp%253a%252f%252futp.sberbank-ast.ru%252f%26wreply%3daHR0cDovL3V0cC5zYmVyYmFuay1hc3QucnUv&wa=wsignin1.0&wtrealm=http%3a%2f%2futp.sberbank-ast.ru%2f&wreply=aHR0cDovL3V0cC5zYmVyYmFuay1hc3QucnUv");

            postRequest.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));

            response = httpClient.execute(postRequest);

            System.out.println("Cookie : ");
            for (Cookie cok : cookies.getCookies()) {
                System.out.println(cok.getName() + ":" + cok.getValue());
            }

            int stCode = response.getStatusLine().getStatusCode();

        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            Singleton.getInstance().getFormController().setStatus(ex.toString());
        }
        return response;
    }

    public CloseableHttpClient createAcceptSelfSignedCertificateClient() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

        SSLContext context = SSLContexts.custom()
                .loadTrustMaterial(TrustSelfSignedStrategy.INSTANCE)
                .build();

        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https", new SSLConnectionSocketFactory(context, NoopHostnameVerifier.INSTANCE))
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);

        HttpHost proxy = new HttpHost("localhost", 8888, HttpHost.DEFAULT_SCHEME_NAME);
        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
        HttpClient httpClient = HttpClients.custom()
                .setConnectionManager(connectionManager)
                //                .setRoutePlanner(routePlanner) //Для фиддлера
                .setDefaultCookieStore(cookies)
                .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
                .build();

        return (CloseableHttpClient) httpClient;
    }

    public List<NameValuePair> getParmFromForm(Element form) {
        List<NameValuePair> lParm = new ArrayList<>();
        Elements eInputs = form.getElementsByTag("input");
        for (Element eInput : eInputs) {
            if (!eInput.attr("type").equals("submit")) {
                lParm.add(new BasicNameValuePair(eInput.attr("name"), eInput.attr("value")));
            } else {
            }
        }
        return lParm;
    }

    public String signValue(String text, Tocken tocken) {
        //TODO запасной вариант подписи. Не находит компоненты. Удалять не стал....
        String result = null;
        ActiveXComponent signer = new ActiveXComponent("CAPICOM.Signer");

        signer.setProperty("Certificate", tocken.getCertificate());
        ActiveXComponent attribute = new ActiveXComponent("CAPICOM.Attribute");
        ActiveXComponent sData = new ActiveXComponent("CAPICOM.SignedData");
        int CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME = 0;

        attribute.setProperty("Name", CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME);
        attribute.setProperty("Value", new Variant(new Date()));

        Dispatch dAuthenticatedAttributes;
        dAuthenticatedAttributes = signer.getProperty("AuthenticatedAttributes").getDispatch();
        Dispatch.call(dAuthenticatedAttributes, "Add", attribute);

        sData.setProperty("Content", text);

        int CAPICOM_ENCODE_BASE64 = 0;

        Variant ret = sData.invoke("Sign", new Variant(signer), new Variant(true), new Variant(CAPICOM_ENCODE_BASE64));
        result = ret.toString();

        return result;
    }

    public String signCadesValue(String text, Tocken tocken) {
        String result = null;
        ActiveXComponent signer = new ActiveXComponent("CAdESCOM.CPSigner");
        ActiveXComponent sData = new ActiveXComponent("CAdESCOM.CadesSignedData");

        signer.setProperty("Certificate", tocken.getCertificate());

        ActiveXComponent attribute = new ActiveXComponent("CAdESCOM.CPAttribute");
        int CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME = 0;
        attribute.setProperty("Name", CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME);
        attribute.setProperty("Value", new Variant(new Date()));

        Dispatch dAuthenticatedAttributes;
        dAuthenticatedAttributes = signer.getProperty("AuthenticatedAttributes2").getDispatch();
        Dispatch.call(dAuthenticatedAttributes, "Add", attribute);

        sData.setProperty("Content", new Variant(text));

        int CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN = 1;
        signer.setProperty("Options", new Variant(CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN));

        int CAPICOM_ENCODE_BASE64 = 0;
        int CADESCOM_CADES_BES = 1;

        try {
            Variant ret = sData.invoke("SignCades", new Variant(signer), new Variant(CADESCOM_CADES_BES), new Variant(true), new Variant(CAPICOM_ENCODE_BASE64));
            result = ret.toString();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            Singleton.getInstance().getFormController().setStatus(ex.toString());
        }

        return result;
    }

    //Создание XML документа из строки
    public Document loadXMLFromString(String xml) {
        Document doc = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            doc = builder.parse(is);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка парсинга документа");
        }
        return doc;
    }

    public HttpResponse sendJSON(CloseableHttpClient httpClient, String aUrl, List<NameValuePair> params) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpResponse response = null;

        try {
            HttpPost postRequest;

            URI urlObject = null;
            try {
                urlObject = new URI(aUrl);
            } catch (URISyntaxException ex) {
                logger.log(Level.SEVERE, null, ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка URL:" + ex);
            }

            postRequest = new HttpPost(aUrl);
            postRequest.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));

//            int timeoutSeconds = 60;
//            int CONNECTION_TIMEOUT_MS = timeoutSeconds * 1000; // Timeout in millis.
//            RequestConfig requestConfig = RequestConfig.custom()
//                    .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
//                    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
//                    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
//                    .build();
//            postRequest.setConfig(requestConfig);
//            postRequest.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");
//            postRequest.addHeader("Accept-Encoding", "gzip, deflate, br");
//            postRequest.addHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
//            postRequest.addHeader("Connection", "keep-alive");
            postRequest.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36");
//            postRequest.addHeader("Origin", "https://login.sberbank-ast.ru");
            postRequest.addHeader("Upgrade-Insecure-Requests", "1");
//            postRequest.addHeader("Cache-Control", "max-age=0");
//            postRequest.addHeader("Referer", "https://login.sberbank-ast.ru/Login.aspx?ReturnUrl=%2f%3fwa%3dwsignin1.0%26wtrealm%3dhttp%253a%252f%252futp.sberbank-ast.ru%252f%26wreply%3daHR0cDovL3V0cC5zYmVyYmFuay1hc3QucnUv&wa=wsignin1.0&wtrealm=http%3a%2f%2futp.sberbank-ast.ru%2f&wreply=aHR0cDovL3V0cC5zYmVyYmFuay1hc3QucnUv");

            response = httpClient.execute(postRequest);

            System.out.println("Cookie : ");
            for (Cookie cok : cookies.getCookies()) {
                System.out.println(cok.getName() + ":" + cok.getValue());
            }

            int stCode = response.getStatusLine().getStatusCode();

        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            Singleton.getInstance().getFormController().setStatus(ex.toString());
        }
        return response;
    }

}
