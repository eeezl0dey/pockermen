/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pockermen.html;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javafx.util.Pair;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.hibernate.Session;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pockermen.Provider;
import pockermen.Singleton;
import pockermen.Singleton.Prov;
import pockermen.StatusStake;
import pockermen.Tocken;
import pockermen.database.Stakelist;

/**
 *
 * @author Evgeniy
 */
public class SberHTMLparser extends BaseHTMLparser {

    public SberHTMLparser() {
        super();
        try {
            FileHandler logFileHandler;
            logFileHandler = new FileHandler(LOG_PATH);
            SimpleFormatter sf = new SimpleFormatter();
            logFileHandler.setFormatter(sf);
            logger = Logger.getLogger(SberHTMLparser.class.getName());
            logger.addHandler(logFileHandler);
        } catch (IOException ex) {
            Logger.getLogger(SberHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(SberHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
        }

        ProviderName = "Сбербанк";
        prov = Prov.Sberbank;
    }

    public class Offer {

        private String sHash;
        private String sXmlData;
        private String sRequestNo;
        private String sOrgName;
        private Stakelist stake;
        private List<NameValuePair> params = null;

        public Offer(Stakelist aStake, String aRequestNo, String aOrgName) {
            sRequestNo = aRequestNo;
            sOrgName = aOrgName;
            stake = aStake;
        }

        public void make() {
            sXmlData = String.format(""
                    + "<mynewpricecontainer>"
                    + "<mynewprice>%.2f</mynewprice>"
                    + "<reqid>%s</reqid>"
                    + "<requestno>%s</requestno>"
                    + "<pricesign>1.00</pricesign>"
                    + "<purchaseid>%s</purchaseid>"
                    + "<purchasecode>%s</purchasecode>"
                    + "<purchasename>%s</purchasename>"
                    + "<suppliername>%s</suppliername>"
                    + "</mynewpricecontainer>",
                    getStake().getPrice(),
                    getStake().getReqID(), sRequestNo, getStake().getPurchID(), getStake().getPurchCode(), getStake().getPurchName(), sOrgName);
            Tocken tocken = findTocken(getStake().getEcp());
            setsHash(signCadesValue(getsXmlData(), tocken));
            params = new ArrayList<>();
            getParams().add(new BasicNameValuePair("reqID", stake.getReqID()));
            getParams().add(new BasicNameValuePair("xmlData", sXmlData));
            getParams().add(new BasicNameValuePair("Hash", getsHash()));
        }

        /**
         * @return the sHash
         */
        public String getsHash() {
            return sHash;
        }

        /**
         * @return the sXmlData
         */
        public String getsXmlData() {
            return sXmlData;
        }

        /**
         * @return the stake
         */
        public Stakelist getStake() {
            return stake;
        }

        /**
         * @param sHash the sHash to set
         */
        public void setsHash(String sHash) {
            this.sHash = sHash;
        }

        /**
         * @return the params
         */
        public List<NameValuePair> getParams() {
            return params;
        }
    }

    @Override
    public synchronized void loadTime() {
        String sUrl = "http://www.sberbank-ast.ru/HttpHandlers/GetDatetime44.ashx";
        CloseableHttpClient httpClient = null;

        String stime = null;
        try {
            //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
            httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();
            stime = httpResponseContent(getUrl(httpClient, sUrl));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка запроса к серверу времени", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка запроса к серверу времени: " + ex);
            return;
        }
        try {
            if (httpClient != null) {
                httpClient.close();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Ошибка закрытия конекта", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка закрытия конекта: " + ex);
        }
        System.out.println(stime);
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date dtime = null;
        try {
            dtime = format.parse(stime);
            Singleton.getInstance().getFormController().setStatus("Время определено");
        } catch (ParseException ex) {
            logger.log(Level.SEVERE, "Ошибка формата времени", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка формата времени");
            return;
        }
        Date curTime = new Date();
        provider.setDifftime(dtime.getTime() - curTime.getTime());
    }

    @Override
    public void login(Tocken tocken) {
        String sLoginUrl = "https://login.sberbank-ast.ru";
        String sSberUtpUrl = "http://utp.sberbank-ast.ru";
        CloseableHttpClient httpClient = null;
        Document document = null;
        String result = null;

        Singleton.getInstance().getFormController().setStatus("Авторизация с токеном: " + tocken);

        //Запрашиваем параметры самого аукционного зала для JSON запроса
        try {
            //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
            httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка открытия коннекта", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка открытия коннекта: " + ex);
            return;
        }

//        String sRequestSberAst = null;
//        try {
//            sRequestSberAst = getUrl(sSberAstUrl);
//        } catch (Exception ex) {
//            Logger.getLogger(SberHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
//            Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
//            return;
//        }
////        System.out.println(sSberAstUrl);
//        document = Jsoup.parse(sRequestSberAst);
//        String tmpUrl = document.getElementById("ctl00_loginctrl_anchSignOn").attr("href");
        HttpResponse response = null;
        try {
            response = getUrl(httpClient, sSberUtpUrl);
            result = httpResponseContent(response);
            document = Jsoup.parse(result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка запроса", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
        }

        Singleton.getInstance().getFormController().setStatus("Проверка на существующую авторизацию");

        Element currentUserFio = document.getElementById("CurrentUserFIO");
        String uname = "";
        if (currentUserFio != null) {
            uname = currentUserFio.text();
        }
        //webDriver.findElementByXPath(".//*[@id='CurrentUserFIO']").getText();
        if (tocken.getName().equals(uname)) {
            Singleton.getInstance().getFormController().setStatus("Пользователь уже в системе");
            return;
        } else if (uname.length() > 0) {
            logout();
        }

        try {
            response = getUrl(httpClient, sLoginUrl + "/Login.aspx");
            result = httpResponseContent(response);
            document = Jsoup.parse(result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка выхода пользователя из системы", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка выхода пользователя из системы: " + ex);
            try {
                httpClient.close();
            } catch (IOException ex1) {
                logger.log(Level.SEVERE, null, ex1);
            }
            return;
        }

//        Uri uri = new Uri(htmlDocument.GetElementbyId("ctl00_loginctrl_anchSignOn").Attributes["href"].Value);
//            string url = string.Format("{0}{1}&ReturnUrl={2}", "https://login.sberbank-ast.ru/Login.aspx", uri.Query, HttpUtility.UrlEncode(uri.PathAndQuery));
        Element form1 = document.getElementById("form1");
        //Готовим адрес запроса
        String sUrlAuth;
        sUrlAuth = form1.attr("action");
        sUrlAuth = sLoginUrl + sUrlAuth.substring(1);
        URL urlAuth = null;

        try {
            urlAuth = new URL(sUrlAuth);
        } catch (MalformedURLException ex) {
            logger.log(Level.SEVERE, "Ошибка URL", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка URL: " + ex);
        }

        //Готовим данные к POST запросу авторизации
        List<NameValuePair> lParm = new ArrayList<>();

        String value;
        //__VIEWSTATE
        value = form1.getElementById("__VIEWSTATE").attr("value");

        lParm.add(
                new BasicNameValuePair("__VIEWSTATE", value));
        //__VIEWSTATEGENERATOR
        value = form1.getElementById("__VIEWSTATEGENERATOR").attr("value");

        lParm.add(
                new BasicNameValuePair("__VIEWSTATEGENERATOR", value));
        //ctl00$mainContent$RadioButtonList2
        value = form1.getElementById("mainContent_RadioButtonList2_0").attr("value");

        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$RadioButtonList2", value));
        //ctl00$mainContent$txtLoginName
        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$txtLoginName", ""));
        //ctl00$mainContent$txtPassword
        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$txtPassword", ""));
        //ctl00$mainContent$DDL1
        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$DDL1", tocken.getThumbprint().toUpperCase()));
        //ctl00$mainContent$btnSubmit
        value = form1.getElementById("mainContent_btnSubmit").attr("value");

        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$btnSubmit", value));
        //ctl00$mainContent$xmlData
        Element reqLogon = document.getElementById("divReqLogon");
        String sRequest = reqLogon.child(0).text();
        String sNow = reqLogon.getElementById("hiddenNow").text();
        String sTicket = reqLogon.getElementById("hiddenTicket").text();
        String xmlData = String.format("<logonregister><request>%s</request><now>%s</now><ticket>%s</ticket></logonregister>",
                sRequest, sNow, sTicket);

        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$xmlData", xmlData));
        //ctl00$mainContent$signValue
        String sSignValue = signCadesValue(xmlData, tocken);
        if (sSignValue == null) {
            //Закроем коннект
            try {
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Ошибка закрытия конекта", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка закрытия конекта: " + ex);
            }
            return;
        }

        lParm.add(
                new BasicNameValuePair("ctl00$mainContent$signValue", sSignValue));

//            getMyPost(lParm,urlAuth);
        for (Header h
                : response.getAllHeaders()) {
            System.out.println(h.getName() + ": " + h.getValue());
        }

        //Отправим основной пост запрос
        try {
            HttpResponse responsePost = postUrl(httpClient, sUrlAuth, lParm);
            result = httpResponseContent(responsePost);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка авторизации", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка авторизации: " + ex);
        }

        //Перейдем на страницу и возмем адрес редиректа
        result = redirectAfterPost(httpClient, sLoginUrl, result);

        //Второй запрос авторизации
        lParm = new ArrayList<>();
        document = Jsoup.parse(result);
        Elements forms = document.getElementsByTag("form");

        if (!forms.isEmpty()) {
            //Готовим параметры второго запроса
            Element form = forms.get(0);
            sUrlAuth = form.attr("action");
            Elements inputs = form.getElementsByTag("input");
            for (Element input : inputs) {
                String atrName = input.attr("name");
                if (atrName.length() > 0) {
                    String atrValue = input.attr("value");
                    lParm.add(new BasicNameValuePair(atrName, atrValue));
                }
            }
            try {
                result = httpResponseContent(postUrl(httpClient, sSberUtpUrl, lParm));
                result = redirectAfterPost(httpClient, sSberUtpUrl, result);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка второго этапа авторизации", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка второго этапа авторизации: " + ex);
            }

        } else {
            logger.log(Level.SEVERE, null, "Ошибка авторизации. Не опознан ответ сервера");
            Singleton.getInstance().getFormController().setStatus("Ошибка авторизации токена " + tocken.toString() + " на сервере " + urlAuth.getHost() + ". Не опознан ответ сервера");
        }

//        try {
//            response = getUrl(httpClient, sSberAstUrl + "/tradezone/default.aspx");
//        } catch (Exception ex) {
//            Logger.getLogger(SberHTMLparser.class.getName()).log(Level.SEVERE, null, ex);
//            Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
//            return;
//        }
        //Закроем коннект
        try {
            if (httpClient != null) {
                httpClient.close();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Ошибка закрытия конекта", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка закрытия конекта: " + ex);
        }

//        for (NameValuePair pair : lParm) {
//            System.out.println(pair.getName() + ": " + pair.getValue());
//        }
//        System.out.println(result);
    }

    public String redirectAfterPost(CloseableHttpClient httpClient, String domain, String textHtml) {
        String result = null;
        Document document = Jsoup.parse(textHtml);
        Elements hrefs = document.getElementsByAttribute("href");
        if (!hrefs.isEmpty()) {
            String tmpUrl = null;
            try {
                //hrefs.get(0).attr("href")
                tmpUrl = domain + URLDecoder.decode(hrefs.get(0).attr("href"), "utf-8");
                result = httpResponseContent(getUrl(httpClient, tmpUrl));

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка редиректа", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка редиректа: " + ex);
            }

        }
        return result;
    }

    @Override
    public void logout(CloseableHttpClient httpClient) {
        Singleton.getInstance().getFormController().setStatus("Выход из учетки");
        String url = "http://utp.sberbank-ast.ru//Main/Util/Logoff";
        try {
            getUrl(httpClient, url);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка выхода из учетной записп", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка выхода из учетной записп: " + ex);
        }
        cookies.clear();
    }

    @Override
    public void logout() {
        try {
            //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
            CloseableHttpClient httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();
            logout(httpClient);
            httpClient.close();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка выхода из учетной записп", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка выхода из учетной записп: " + ex);
            return;
        }

    }

    @Override
    protected void checkStake(Stakelist stake, boolean isDetail) {
        //Проверка статуса
        CloseableHttpClient httpClient = null;
        StatusStake.EStatus status = StatusStake.castIdToEStatus(stake.getIdStatus().getId());
        String result;

        try {
            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isLoadingParm));
            Singleton.getInstance().getFormController().refreshData();

            Singleton.getInstance().getFormController().setStatus("Проверка ставки: " + stake.getAuction());
            String url = "http://www.sberbank-ast.ru/tradezone/Supplier/PurchaseRequestList.aspx";

            Document document = null;

            try {
                //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
                httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка открытия коннекта", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка открытия коннекта: " + ex);
                return;
            }

            HttpResponse response = null;
            try {
                response = getUrl(httpClient, url);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка запроса", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
            }

            //Проверим по тегу, авторизированы ли
            List<NameValuePair> lParm;
            String sPurchaseRequestList = httpResponseContent(response);
            document = Jsoup.parse(sPurchaseRequestList);
            String title = document.getElementsByTag("title").get(0).text();
            if (title.equals("Working...")) {
                //Готовим данные к POST запросу авторизации
                Element form = document.getElementsByTag("form").get(0);
                String postStrDoActivate = form.attr("action");
                lParm = getParmFromForm(form);
                try {
                    result = httpResponseContent(postUrl(httpClient, postStrDoActivate, lParm));
                    document = Jsoup.parse(result);
//                    result = redirectAfterPost(httpClient, postStrDoActivate, result);

                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Ошибка авторизации", ex);
                    Singleton.getInstance().getFormController().setStatus("Ошибка авторизации: " + ex);
                }

            }

            //Запросим все с формы и лишь подменим номер аукциона для поиска
            lParm = new ArrayList<>();
            Element form = document.getElementById("aspnetForm");
            lParm = getParmFromForm(form);

            for (int i = 0; i < lParm.size(); i++) {
                NameValuePair pair = lParm.get(i);
                if (pair.getName().equals("ctl00$ctl00$phWorkZone$phFilterZone$nbtPurchaseListFilter$tbxPurchaseCode")) {
                    lParm.set(i, new BasicNameValuePair("ctl00$ctl00$phWorkZone$phFilterZone$nbtPurchaseListFilter$tbxPurchaseCode", stake.getAuction()));
                }
                if (pair.getName().equals("ctl00$ctl00$phWorkZone$xmlFilter")) {
                    String xmlFilter = String.format("<query>"
                            + "<purchcode>%s</purchcode>"
                            + "<purchtype></purchtype>"
                            + "<purchname></purchname>"
                            + "<purchamountstart></purchamountstart>"
                            + "<purchamountend></purchamountend>"
                            + "<issmp>-1</issmp>"
                            + "<orgid></orgid>"
                            + "<orgname></orgname>"
                            + "<purchstate></purchstate>"
                            + "<purchbranchid></purchbranchid><"
                            + "purchbranchname></purchbranchname>"
                            + "<regionid></regionid>"
                            + "<regionname></regionname>"
                            + "<publicdatestart></publicdatestart>"
                            + "<publicdateend></publicdateend>"
                            + "<requestdatestart></requestdatestart>"
                            + "<requestdateend></requestdateend>"
                            + "<auctionbegindatestart></auctionbegindatestart>"
                            + "<auctionbegindateend></auctionbegindateend>"
                            + "</query>", stake.getAuction());
                    lParm.set(i, new BasicNameValuePair("ctl00$ctl00$phWorkZone$xmlFilter", xmlFilter));
                    break;
                }

            }

            //Поиск аукциона
            try {
                sPurchaseRequestList = httpResponseContent(postUrl(httpClient, url, lParm));
                document = Jsoup.parse(sPurchaseRequestList);

                title = document.getElementsByTag("title").get(0).text();
                if (title.equals("Working...")) {
                    //Готовим данные к POST запросу авторизации
                    form = document.getElementsByTag("form").get(0);
                    String postStrDoActivate = form.attr("action");
                    try {
                        sPurchaseRequestList = httpResponseContent(postUrl(httpClient, url, lParm));
                        document = Jsoup.parse(sPurchaseRequestList);//                    result = redirectAfterPost(httpClient, postStrDoActivate, result);

                    } catch (Exception ex) {
                        logger.log(Level.SEVERE, "Ошибка авторизации", ex);
                        Singleton.getInstance().getFormController().setStatus("Ошибка авторизации: " + ex);
                    }

                }
//                    result = redirectAfterPost(httpClient, postStrDoActivate, result);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка при поиске аукциона", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка при поиске аукциона: " + ex);
            }

            String sxml = document.getElementById("ctl00_ctl00_phWorkZone_xmlData").text();
            if (sxml.length() > 0) {
                org.w3c.dom.Document xmlDocument = loadXMLFromString(sxml);
                NodeList nodeL;
                //Если детализация, то возмем и эти параметры
                if (isDetail) {
                    //ASID аукциона
                    nodeL = xmlDocument.getElementsByTagName("ASID");
                    String sASID = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                    System.out.println("checkState ASID: " + sASID);
                    stake.setAsid(sASID);

                    //reqID аукциона
                    nodeL = xmlDocument.getElementsByTagName("reqID");
                    String sReqID = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                    System.out.println("checkState reqID: " + sReqID);
                    stake.setReqID(sReqID);
                }
                //Найдем ID аукциона (purchId)
                nodeL = xmlDocument.getElementsByTagName("purchID");
                String spurchid = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                System.out.println("checkState purchid: " + spurchid);
                //Теперь откроем тот же XML с другой страницы но более детальный
                try {
                    String sPurchaseView = httpResponseContent(getUrl(httpClient, "http://www.sberbank-ast.ru/purchaseView.aspx?id=" + spurchid));
                    document = Jsoup.parse(sPurchaseView);
                    sxml = document.getElementById("ctl00_ctl00_phWorkZone_xmlData").text();
                    xmlDocument = loadXMLFromString(sxml);

                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Ошибка запроса детализации", ex);
                    Singleton.getInstance().getFormController().setStatus("Ошибка запроса детализации: " + ex);
                }

                //Время начала торгов                
                nodeL = xmlDocument.getElementsByTagName("AuctionBeginDate");
                String stime = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                nodeL = xmlDocument.getElementsByTagName("AuctionBeginDateTime");
                stime += " " + new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
//                stime = "18.03.2018 12:35";//TODO удалить.
                System.out.println("checkState date_start: " + stime);
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date dtime = null;
                try {
                    dtime = format.parse(stime);
                    Singleton.getInstance().getFormController().setStatus("Время торгов: " + stime);

                } catch (ParseException ex) {
                    logger.log(Level.SEVERE, "Ошибка формата времени", ex);
                    Singleton.getInstance().getFormController().setStatus("Ошибка формата времени: " + stime);
                    return;
                }
                stake.setDateStart(stime);

                //Наименование закупки
                nodeL = xmlDocument.getElementsByTagName("purchname");
                String snameb = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);//nodeL.item(0).getTextContent();
                System.out.println("checkState service name: " + snameb);
                stake.setPurchName(snameb);

                //Код закупки
                nodeL = xmlDocument.getElementsByTagName("purchcode");
                String scodeb = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                System.out.println("checkState service code: " + scodeb);
                stake.setPurchCode(scodeb);

                //Установим контрагента
                nodeL = xmlDocument.getElementsByTagName("orgName");
                String scontr = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                System.out.println("checkState contragent: " + scontr);
                stake.setContractor(scontr);

                //Найдем ID аукциона
                nodeL = xmlDocument.getElementsByTagName("purchid");
                spurchid = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                System.out.println("checkState purchid: " + spurchid);
                stake.setPurchID(spurchid);

                //Максимальная цена
                nodeL = xmlDocument.getElementsByTagName("purchamount");
                String smaxprice = new String(nodeL.item(0).getTextContent().getBytes(), StandardCharsets.UTF_8);
                System.out.println("checkState max price: " + smaxprice);
                Double maxPrice = Double.parseDouble(smaxprice);
                stake.setMaxprice(maxPrice);

                //Проверим на корректность ставки (не может быть больше максимальной)
                if (maxPrice < stake.getPrice()) {
                    //Выставим статус
                    stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":Превышение начальной цены!");
                    stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
                } else {
                    //Выставим статус
                    stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isGetParm));
                }

                Session session = Singleton.getInstance().getSession();
                session.beginTransaction();
                session.update(stake);
                session.getTransaction().commit();
                //Сразу запустим таймер на запуск
                Thread.yield();
                //Если уж удачно запросили прараметры, поставим на исполнение
                if (!isDetail) {
                    preStartStake(stake);
                } else {
                    stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isRunTimer));
                    Singleton.getInstance().getFormController().refreshData();
                }
            } else {
                Singleton.getInstance().getFormController().setStatus("Ставка не найдена");
                Session session = Singleton.getInstance().getSession();
                stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
                stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":Ставка не найдена!");
                session.beginTransaction();
                session.update(stake);
                session.getTransaction().commit();

            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка запроса ставки", ex);
            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
            stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":Возникло исключение в запросе ставки!");
            Singleton.getInstance().getFormController().setStatus("Ошибка запроса ставки: " + stake.getAuction());
            Session session = Singleton.getInstance().getSession();
            session.beginTransaction();
            session.update(stake);
            session.getTransaction().commit();
        } finally {
            try {
                httpClient.close();

            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
        Singleton.getInstance().getFormController().refreshData();
    }

    @Override
    public void timerStake(Stakelist stake) {
        stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isRunTimer));
        Singleton.getInstance().getFormController().setStatus("Установка таймера аукциона: " + stake.getAuction());
        //Прибавим разницу с нашей машиной и вычтем время для заблаговременного старта
        Date stime = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            //Переведем дату в документе в формат
            String startDate = stake.getDateStart();
            System.out.println("processingStake парсинг даты:" + startDate);
            stime = format.parse(startDate);

        } catch (ParseException ex) {
            String sErr = "Ошибка формата времени при постановке таймера";
            logger.log(Level.SEVERE, sErr, ex);
            Singleton.getInstance().getFormController().setStatus("Аукцион: " + stake.getAuction() + " - " + sErr);
            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
            stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":" + sErr);
            Singleton.getInstance().getFormController().refreshData();
            return;
        }

        //Подготовим предложение
        CloseableHttpClient httpClient = null;
        Document document = null;

        try {
            //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
            httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка открытия коннекта аукциона", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка открытия коннекта аукциона: " + ex);
            return;
        }

        Singleton.getInstance().getFormController().setStatus("Получение номера заявки аукциона: " + stake.getAuction());
        String requestNumber = null;

        HttpResponse response = null;
        String sUrl;
        try {
            sUrl = "http://www.sberbank-ast.ru/tradezone/Supplier/PurchaseRequestRecall.aspx";
            List<NameValuePair> lParm = new ArrayList<>();
            lParm.add(new BasicNameValuePair("reqid", stake.getReqID()));
            response = getUrl(httpClient, sUrl, lParm);

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Ошибка запроса", ex);
            Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
        }
        String sRequest = httpResponseContent(response);
        document = Jsoup.parse(sRequest);
        String sXML = document.getElementById("ctl00_ctl00_phWorkZone_xmlData").text();
        org.w3c.dom.Document xmlDocument = loadXMLFromString(sXML);
        NodeList nodeL = xmlDocument.getElementsByTagName("reqno");
        requestNumber = nodeL.item(0).getTextContent();

        //и название организации
        String sOrgName = document.getElementById("ctl00_ctl00_loginctrl_link").text();

//                Http.Routine.HttpParameter parameter = new Http.Routine.HttpParameter("reqid", auctionInfo.RequestId);
//                Http.Routine.HttpParameter[] parameters = new Http.Routine.HttpParameter[] { parameter };
//                Http.Routine.HttpResponse response = Http.Routine.Http.Get(string.Format("{0}/{1}", "http://www.sberbank-ast.ru", "tradezone/Supplier/PurchaseRequestRecall.aspx"), this.Cookies, parameters);
//                HtmlDocument document1 = new HtmlDocument();
//                document1.LoadHtml(response.Response);
//                string xml = HttpUtility.HtmlDecode(document1.GetElementbyId("ctl00_ctl00_phWorkZone_dataPanel").ChildNodes[1].InnerText);
//                XmlDocument document2 = new XmlDocument();
//                document2.LoadXml(xml);
//                num = int.Parse(document2["supplierrequestrecall"]["reqno"].InnerText);
//                        base.OnMessage("Номер заявки получен");
        Offer offer = new Offer(stake, requestNumber, sOrgName);
        offer.make();

        int startBeforeSec = 1;//за ... секунд
        Long timeStart = stime.getTime() - provider.getDifftime() - startBeforeSec * 1000;
        //Запустим само ожидание
        Date curTime = new Date();
        Date startDate = new Date(timeStart);
        while (curTime.getTime() < timeStart) {
            Thread.yield();
            try {
                Thread.sleep(1);

            } catch (InterruptedException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            curTime = new Date();
        }
        //Если дождались, то запустим сам процесс формирования и отправки
        sendOfferJSON(offer);

    }

    protected void sendOfferJSON(Offer offer) {
        offer.getStake().setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isAction));
        Singleton.getInstance().getFormController().refreshData();

        Singleton.getInstance().getFormController().setStatus("Генерируем заявку");
        Stakelist stake = offer.getStake();
        String sUrl = String.format("http://www.sberbank-ast.ru/tradezone/Supplier/TradePlace.aspx/AsyncRefresh?reqid=%s&ASID=%s", stake.getReqID(), stake.getAsid());

        CloseableHttpClient httpClient = null;
        StatusStake.EStatus status = StatusStake.castIdToEStatus(stake.getIdStatus().getId());
        String result;

        try {
            Document document = null;

            try {
                //httpClient = (CloseableHttpClient) createHttpClient_AcceptsUntrustedCerts();
                httpClient = (CloseableHttpClient) createAcceptSelfSignedCertificateClient();

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Ошибка открытия коннекта", ex);
                Singleton.getInstance().getFormController().setStatus("Ошибка открытия коннекта: " + ex);
                return;
            }

            HttpResponse response = null;

            //Отправляем 2 секунды
            long timeSent = 2 * 1000;
            Date sDate = new Date();
            Date curDate = null;
            do {
                try {
                    response = sendJSON(httpClient, sUrl, offer.getParams());
                    Singleton.getInstance().getFormController().setStatus("Заявка отправлена");
                    String sRequest = httpResponseContent(response);
                    document = Jsoup.parse(sRequest);
                    Thread.sleep(10);
                    Thread.yield();
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Ошибка запроса", ex);
                    Singleton.getInstance().getFormController().setStatus("Ошибка запроса: " + ex);
                }
                curDate = new Date();
            } while ((curDate.getTime() - sDate.getTime()) < timeSent);

            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isWiteResult));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Исключение ставки аукциона", ex);
            stake.setIdStatus(StatusStake.getStakestatusFromEStatus(StatusStake.EStatus.isError));
            stake.setStatusComment(StatusStake.castEStatusToId(StatusStake.EStatus.isError) + ":Возникло исключение ставки аукциона!");
            Singleton.getInstance().getFormController().setStatus("Исключение ставки аукциона: " + stake.getAuction());
        } finally {
            try {
                httpClient.close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            
            finish();
        }
        
        Session session = Singleton.getInstance().getSession();
        session.beginTransaction();
        session.update(stake);
        session.getTransaction().commit();

        Singleton.getInstance().getFormController().setStatus("Заявки отправлены");
        Singleton.getInstance().getFormController().refreshData();
    }

}
